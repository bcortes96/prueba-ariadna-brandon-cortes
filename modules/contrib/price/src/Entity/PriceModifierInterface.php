<?php

namespace Drupal\price\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for entity.
 */
interface PriceModifierInterface extends ConfigEntityInterface {

}
