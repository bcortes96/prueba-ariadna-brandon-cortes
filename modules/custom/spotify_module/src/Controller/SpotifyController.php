<?php

namespace Drupal\spotify_module\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines SpotifyController class.
 */
class SpotifyController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function viewReleases() {
  
    //Spotify service
    $service = \Drupal::service('spotify_module.spotify_call');
    $json = $service->getReleases();
    $arrayReleases = json_decode($json,true);

    return [
      '#theme' => 'spotify_releases',
      '#releases' => $arrayReleases
    ];
    
  }

  public function viewArtist($id = NULL) {
  
    //Spotify service
    $service = \Drupal::service('spotify_module.spotify_call');
    $jsonArtist = $service->getArtist($id);
    $arrayArtist = json_decode($jsonArtist,true);

    $jsonArtistAlbum = $service->getArtistAlbum($id);
    $arrayArtistAlbum = json_decode($jsonArtistAlbum,true);


    return [
      '#theme' => 'spotify_artist',
      '#artist' => $arrayArtist,
      '#album' => $arrayArtistAlbum
    ];
  }

}