<?php
namespace  Drupal\spotify_module;



class SpotifyApi {

    public function __construct() {
        $client_id = '78f13e0738a8458a882e7f62d1248b9e';
        $client_secret = '7fe566f4f22b48daaf7e3f223373977d';

        $this->client = \Drupal::httpClient();
        $this->login =  $this->client->request('POST','https://accounts.spotify.com/api/token' , [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $client_id,
                'client_secret' => $client_secret
            ]
        ]);
    }

    public function getReleases(){
        $response = json_decode($this->login->getBody());
                
                
        //obtener ultimos lanzamientos
        
        $request = $this->client->request('GET', 'https://api.spotify.com/v1/browse/new-releases' , [
            'headers' => [
                'Authorization' => $response->token_type.' '.$response->access_token
            ]
        ]);
        
        $lastReleases = $request->getBody();

        return $lastReleases;
    }

    public function getArtist($id){
        $response = json_decode($this->login->getBody());
                
                
        //obtener artista
        
        $request = $this->client->request('GET', 'https://api.spotify.com/v1/artists/'.$id , [
            'headers' => [
                'Authorization' => $response->token_type.' '.$response->access_token
            ]
        ]);
        
        $artist = $request->getBody();

        return $artist;
    }    

    public function getArtistAlbum($id){
        $response = json_decode($this->login->getBody());
                
                
        //obtener albunes del artista
        
        $request = $this->client->request('GET', 'https://api.spotify.com/v1/artists/'.$id.'/albums', [
            'headers' => [
                'Authorization' => $response->token_type.' '.$response->access_token
            ]
        ]);
        
        $artistAlbum = $request->getBody();

        return $artistAlbum;
    }     

}
